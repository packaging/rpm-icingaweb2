#!/bin/bash
# this script runs in the rpm_test environment

SCRIPT_HOME="$(dirname "$(readlink -f "$0")")"

if ! $SCRIPT_HOME/prepare_test.sh; then
  exit 1
fi

IP_ADDRESS="$(hostname -I | cut -d' ' -f1)"
echo "Icinga Web is now available at http://$IP_ADDRESS/icingaweb2"
echo

bash
